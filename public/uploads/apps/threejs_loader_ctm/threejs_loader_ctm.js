// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014



function addScriptForThreejs( url, callback ) {
	var script = document.createElement( 'script' );
	if( callback ) script.onload = callback;
	script.type = 'text/javascript';
	script.src = url;
	document.body.appendChild( script );  
}


var threejs_loader_ctm = SAGE2_App.extend( {
	construct: function() {
		arguments.callee.superClass.construct.call(this);

		this.width  = null;
		this.height = null;
		this.resizeEvents = "continuous";

		this.renderer = null;
		this.camera   = null;
		this.scene    = null;
		this.controls = null;
		this.ready    = null;

		this.cameraCube = null;
		this.sceneCube  = null;
		this.dragging   = null;
		this.rotating   = null;
	},

	init: function(data) {
		// call super-class 'init'
		arguments.callee.superClass.init.call(this, "div", data);
	
		this.element.id = "div" + data.id;
		this.frame  = 0;
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		this.dragging = false;
		this.ready    = false;
		this.rotating = false;

		var _this = this;
		addScriptForThreejs(_this.resrcPath + "scripts/OrbitControls.js", function() {
			addScriptForThreejs(_this.resrcPath + "scripts/ctm/lzma.js", function() {
				addScriptForThreejs(_this.resrcPath + "scripts/ctm/ctm.js", function() {
					addScriptForThreejs(_this.resrcPath + "scripts/ctm/CTMLoader.js", function() {
						_this.initialize(data.date);
					});
				});
			});
		});
		this.controls.addButton({type:"prev",sequenceNo:7,action:function(date){ 
			this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
			this.orbitControls.update();
			this.refresh(date);
		}.bind(this)});
		this.controls.addButton({type:"next",sequenceNo:1,action:function(date){ 
			// right
			this.orbitControls.pan(  - this.orbitControls.keyPanSpeed, 0);
			this.orbitControls.update();
			this.refresh(date);
		}.bind(this)});
		this.controls.addButton({type:"up-arrow",sequenceNo:4,action:function(date){ 
			// up
			this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
			this.orbitControls.update();
			this.refresh(date);
		}.bind(this)});
		this.controls.addButton({type:"down-arrow",sequenceNo:10,action:function(date){ 
			// down
			this.orbitControls.pan(0, - this.orbitControls.keyPanSpeed);
			this.orbitControls.update();
			this.refresh(date);
		}.bind(this)});
				
		this.controls.addButton({type:"zoom-in",sequenceNo:5,action:function(date){ 
			this.orbitControls.scale(4);
			this.refresh(date);
		}.bind(this)});
		this.controls.addButton({type:"zoom-out",sequenceNo:6,action:function(date){ 
			this.orbitControls.scale(-4);
			this.refresh(date);
		}.bind(this)});
		this.controls.addButton({type:"loop",sequenceNo:8,action:function(date){ 
			this.rotating = ! this.rotating;
			this.orbitControls.autoRotate = this.rotating;
			this.refresh(date);
		}.bind(this)});
		this.controls.finishedAddingControls();
	},

	initialize: function(date) {
		// CAMERA
		this.camera = new THREE.PerspectiveCamera( 25, this.width / this.width, 1, 10000 );
		this.camera.position.set( 185, 40, 170 );

		this.orbitControls = new THREE.OrbitControls( this.camera, this.element );
		this.orbitControls.maxPolarAngle = Math.PI / 2;
		this.orbitControls.minDistance = 200;
		this.orbitControls.maxDistance = 500;
		this.orbitControls.autoRotate  = false; //true;
		this.orbitControls.zoomSpeed   = 0.1;
		this.orbitControls.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

		// SCENE
		this.scene = new THREE.Scene();

		// SKYBOX
		this.sceneCube  = new THREE.Scene();
		this.cameraCube = new THREE.PerspectiveCamera( 25, this.width / this.width, 1, 10000 );
		this.sceneCube.add( this.cameraCube );

		var r    = this.resrcPath + "textures/";
		var urls = [ r + "px.jpg", r + "nx.jpg", r + "py.jpg", r + "ny.jpg", r + "pz.jpg", r + "nz.jpg" ];
		var textureCube = THREE.ImageUtils.loadTextureCube( urls );

		var shader = THREE.ShaderLib[ "cube" ];
		shader.uniforms[ "tCube" ].value = textureCube;

		var material = new THREE.ShaderMaterial( {
			fragmentShader: shader.fragmentShader,
			vertexShader:   shader.vertexShader,
			uniforms:       shader.uniforms,
			depthWrite:     false,
			side:           THREE.BackSide
		} );

		var mesh = new THREE.Mesh( new THREE.BoxGeometry( 100, 100, 100 ), material );
		this.sceneCube.add( mesh );

		// LIGHTS

		var light = new THREE.PointLight( 0xffffff, 1 );
		light.position.set( 2, 5, 1 );
		light.position.multiplyScalar( 30 );
		this.scene.add( light );

		var light = new THREE.PointLight( 0xffffff, 0.75 );
		light.position.set( -12, 4.6, 2.4 );
		light.position.multiplyScalar( 30 );
		this.scene.add( light );

		this.scene.add( new THREE.AmbientLight( 0x050505 ) );

		// RENDERER
		this.renderer = new THREE.WebGLRenderer( { antialias: true } );
		this.renderer.setSize(this.width, this.height);
		this.renderer.autoClear = false;

		this.element.appendChild(this.renderer.domElement);
		
		this.renderer.gammaInput  = true;
		this.renderer.gammaOutput = true;

		// LOADER
		var start = Date.now();

		loaderCTM = new THREE.CTMLoader( true );
		//document.body.appendChild( loaderCTM.statusDomElement );

		var position = new THREE.Vector3( -105, -78, -40 );
		var scale    = new THREE.Vector3( 30, 30, 30 );

		var _this = this;
		loaderCTM.loadParts( _this.resrcPath + "camaro/camaro.js", function( geometries, materials ) {
			// hackMaterials
			for ( var i = 0; i < materials.length; i ++ ) {
				var m = materials[ i ];
				if ( m.name.indexOf( "Body" ) !== -1 ) {
					var mm = new THREE.MeshPhongMaterial( { map: m.map } );
					mm.envMap  = textureCube;
					mm.combine = THREE.MixOperation;
					mm.reflectivity = 0.75;
					materials[ i ] = mm;
				} else if ( m.name.indexOf( "mirror" ) !== -1 ) {
					var mm = new THREE.MeshPhongMaterial( { map: m.map } );
					mm.envMap  = textureCube;
					mm.combine = THREE.MultiplyOperation;
					materials[ i ] = mm;
				} else if ( m.name.indexOf( "glass" ) !== -1 ) {
					var mm = new THREE.MeshPhongMaterial( { map: m.map } );
					mm.envMap = textureCube;
					mm.color.copy( m.color );
					mm.combine = THREE.MixOperation;
					mm.reflectivity = 0.25;
					mm.opacity = m.opacity;
					mm.transparent = true;
					materials[ i ] = mm;
				} else if ( m.name.indexOf( "Material.001" ) !== -1 ) {
					var mm = new THREE.MeshPhongMaterial( { map: m.map } );
					mm.shininess = 30;
					mm.color.setHex( 0x404040 );
					mm.metal = true;
					materials[ i ] = mm;
				}
				materials[ i ].side = THREE.DoubleSide;
			}

			for ( var i = 0; i < geometries.length; i ++ ) {
				var mesh = new THREE.Mesh( geometries[ i ], materials[ i ] );
				mesh.position.copy( position );
				mesh.scale.copy( scale );
				_this.scene.add( mesh );
			}

			// loaderCTM.statusDomElement.style.display = "none";

			var end = Date.now();
			console.log( "load time:", end - start, "ms" );

		}, { useWorker: true } );
		
		this.ready = true;

		// draw!
		this.resize(date);
	},
	
	load: function(state, date) {
	},

	draw: function(date) {
		if (this.ready) {
			this.orbitControls.update();
			this.cameraCube.rotation.copy( this.camera.rotation );

			this.renderer.clear();
			this.renderer.render( this.sceneCube, this.cameraCube );
			this.renderer.render( this.scene, this.camera );
		}
	},

	resize: function(date) {
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		this.renderer.setSize(this.width, this.height);
		
		this.camera.aspect = this.width / this.height;
		this.camera.updateProjectionMatrix();

		this.cameraCube.aspect = this.width / this.height;
		this.cameraCube.updateProjectionMatrix();

		this.refresh(date);
	},
	
	event: function(eventType, position, user_id, data, date) {
		if (this.ready) {
			if (eventType === "pointerPress" && (data.button === "left")) {
				this.dragging = true;
				this.orbitControls.mouseDown(position.x,position.y,0);
			}
			else if (eventType === "pointerMove" && this.dragging) {
				this.orbitControls.mouseMove(position.x, position.y);
				this.refresh(date);
			}
			else if (eventType === "pointerRelease" && (data.button === "left")) {
				this.dragging = false;
			}

			if (eventType === "pointerScroll") {
				this.orbitControls.scale( data.wheelDelta );
				this.refresh(date);
			}
			
			if (eventType === "keyboard") {
				if(data.character === " ") {
					this.rotating = ! this.rotating;
					this.orbitControls.autoRotate = this.rotating;
					this.refresh(date);
				}
			}
			
			if (eventType === "specialKey") {
				if (data.code === 37 && data.state === "down") { // left
					this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				}
				else if (data.code === 38 && data.state === "down") { // up
					this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				}
				else if (data.code === 39 && data.state === "down") { // right
					this.orbitControls.pan(  - this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				}
				else if (data.code === 40 && data.state === "down") { // down
					this.orbitControls.pan(0, - this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				}				
			}
		}
	}

});
